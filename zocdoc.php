<?php
//Import functions and extenstions
include('scripts/simple_html_dom.php');
include('scripts/functions.php');
ini_set('memory_limit', '512M');
set_time_limit (0);

//Specific script data
$scriptName = "zocdoc.php";

//common functions and variables
$numberOfDoctors = 0;
$startTime = time();
$northLatitude = 40.770352;
$southLatitude = 40.742769;
$eastLongitude = -73.956122;
$westLongitude = -74.012601;

$runDate = date("Y-m-d H:i:s");

$paramsArray = [
    "operationName"=> "search",
    "variables"=> [
        "includeCustomProviderProps"=> false,
        "includeSpo"=> true,
        "insuranceCarrierId"=> "-1",
        "insurancePlanId"=> "-1",
        "parameters"=> [
            "callerType"=> "search",
            "filters" => [],
            "dayFilter"=> "AnyDay",
            "directoryId"=> "-1",
            "insuranceCarrierId"=> "-1",
            "insurancePlanId"=> "-1",
            "itemOffset"=> 0,
            "itemQuantity"=> 100,
            "latencyTestDelayMs"=> 0,
            "url"=> "https://www.zocdoc.com/search",
            "pageType"=> "SearchPage",
            "pageId"=> "a4c90c6a80476f85",
            "procedureId"=> "-1",
            "rankBy"=> "Default",
            "searchLocation"=> [
                "geoShape"=> [
                	"northLatitude"=> $northLatitude,
                	"southLatitude"=> $southLatitude,
                	"eastLongitude"=> $eastLongitude,
                	"westLongitude"=> $westLongitude
                ]
            ],
            "searchQuery"=> "Dentist",
            "sessionId"=> "763c43bac9cd4f9cbc86a4d24e0a2f88",
            "specialtyId"=> "98",
            "timeFilter"=> "AnyTime",
            "trackingId"=> "d957d985-aa7f-4f92-8dd9-ba8f1f8e11bd",
            "searchType"=> "specialty",
            "locale"=> "en"
        ],
        "specialtyId"=> "98",
        "includeBadges"=> false,
        "includeHasAvailability"=> true,
        "skipAvailability"=> true
    ],
    "query" => "query search(\$directoryId: String, \$includeBadges: Boolean!, \$includeCustomProviderProps: Boolean!, \$includeSpo: Boolean!, \$includeHasAvailability: Boolean!, \$insuranceCarrierId: String!, \$insurancePlanId: String!, \$isNewPatient: Boolean, \$isReschedule: Boolean, \$jumpAhead: Boolean, \$numDays: Int, \$parameters: SearchParameters!, \$procedureId: String, \$searchRequestId: String, \$skipAvailability: Boolean!, \$specialtyId: String!, \$startDate: String, \$timeFilter: TimeFilter, \$widget: Boolean) {\n  insuranceCarrierImage(carrierId: \$insuranceCarrierId)\n  search(parameters: \$parameters) {\n    id\n    searchResponse {\n      nearestState\n      searchRequestId\n      providerLocations {\n        ...providerLocation\n        provider {\n          properties @include(if: \$includeCustomProviderProps) {\n            key\n            value\n            __typename\n          }\n          __typename\n        }\n        __typename\n      }\n      providerLocationsMapDots {\n        providerId\n        locationId\n        latitude\n        longitude\n        hasAvailability @include(if: \$includeHasAvailability)\n        __typename\n      }\n      facets {\n        id\n        facetId\n        displayName\n        isMulti\n        popularOptions {\n          optionId\n          displayName\n          count\n          __typename\n        }\n        selectionRequired\n        options {\n          optionId\n          displayName\n          count\n          __typename\n        }\n        __typename\n      }\n      governmentInsuranceData {\n        showGovernmentInsuranceWarning\n        header\n        message\n        links {\n          name\n          url\n          __typename\n        }\n        __typename\n      }\n      totalCount\n      searchUrl\n      __typename\n    }\n    spo @include(if: \$includeSpo) {\n      spoAds {\n        spoAdDecisionId\n        adDecisionToken\n        adRank\n        providerLocation {\n          ...providerLocation\n          __typename\n        }\n        __typename\n      }\n      telehealthAds {\n        adDecisionId\n        decisionToken\n        provider {\n          allowedPatient\n          cost\n          coupon {\n            couponCode\n            discountedCost\n            __typename\n          }\n          fullName\n          partnerName\n          providerImage\n          readableProviderType\n          branchDeepLink\n          sourceId\n          __typename\n        }\n        searchResultId\n        spoServiceAttributionUrl\n        spoServiceImpressionUrl\n        __typename\n      }\n      showPlushCareAd\n      __typename\n    }\n    searchOrigin {\n      latitude\n      longitude\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment providerLocation on ProviderLocation {\n  id\n  provider {\n    id\n    monolithId\n    approvedFullName\n    averageRating\n    averageBedsideRating\n    averageWaitTimeRating\n    canHaveAppointments\n    frontEndCirclePictureUrl\n    defaultProcedureId\n    firstName\n    isPreview\n    lastName\n    nameInSentence\n    onlySeesChildren\n    postnominal\n    prenominal\n    profileUrl\n    frontEndSquarePictureUrl\n    status\n    relevantSpecialty(searchSpecialtyId: \$specialtyId) {\n      id\n      name\n      __typename\n    }\n    procedures {\n      id\n      __typename\n    }\n    reviewCount\n    representativeReview {\n      comment\n      __typename\n    }\n    specialties {\n      id\n      name\n      __typename\n    }\n    badges @include(if: \$includeBadges)\n    __typename\n  }\n  location {\n    id\n    monolithId\n    address1\n    address2\n    city\n    latitude\n    longitude\n    name\n    phone\n    state\n    zipCode\n    isVirtual\n    __typename\n  }\n  practice {\n    id\n    isUrgentCare\n    __typename\n  }\n  acceptsInsurance(carrierId: \$insuranceCarrierId, planId: \$insurancePlanId)\n  distance(unit: \"mi\")\n  searchResultId\n  ...availability @skip(if: \$skipAvailability)\n  __typename\n}\n\nfragment availability on ProviderLocation {\n  id\n  provider {\n    id\n    monolithId\n    __typename\n  }\n  location {\n    id\n    monolithId\n    state\n    phone\n    __typename\n  }\n  availability(directoryId: \$directoryId, insurancePlanId: \$insurancePlanId, isNewPatient: \$isNewPatient, isReschedule: \$isReschedule, jumpAhead: \$jumpAhead, numDays: \$numDays, procedureId: \$procedureId, searchRequestId: \$searchRequestId, startDate: \$startDate, timeFilter: \$timeFilter, widget: \$widget) {\n    times {\n      date\n      timeslots {\n        isResource\n        startTime\n        __typename\n      }\n      __typename\n    }\n    firstAvailability {\n      startTime\n      __typename\n    }\n    showGovernmentInsuranceNotice\n    timesgridId\n    today\n    __typename\n  }\n  __typename\n}\n"
];

$fileHandle = null;

//BEGIN CRAWLING
$websiteURL = "https://api.zocdoc.com/directory/v2/gql";

$fileHandle = newFile($scriptName);
$csvHeader = "Doctor Name" . "\t" . "Insurance Name" . "\t" . "Insurance Plan";
fwrite($fileHandle, $csvHeader . "\n");

$keepGoing = true;
$retry = 0;

$doctorsId = [];
$doctorsArray = [];

while ($keepGoing) {
    $mainPageHTML = new simple_html_dom;
    $mainPageHTML->load(file_get_contents_curl_post_json($websiteURL, json_encode($paramsArray, JSON_UNESCAPED_SLASHES)));

    $jsonResponse = json_decode($mainPageHTML, true);

    if (isset($jsonResponse['data']['search']['searchResponse']['providerLocations']) && !empty($jsonResponse['data']['search']['searchResponse']['providerLocations'])) {
        $retry = 0;
        foreach ($jsonResponse['data']['search']['searchResponse']['providerLocations'] as $doctorItem) {
            $doctorName = "";
            
            $doctorId = $doctorItem['provider']['id'];
            $doctorName = $doctorItem['provider']['approvedFullName'];
            $link = $doctorItem['provider']['profileUrl'];
            $longitude = $doctorItem['location']['longitude'];
            $latitude = $doctorItem['location']['latitude'];
            
            if (!in_array($doctorId, $doctorsId)) {
                $numberOfDoctors++;
                echo "Parse doctor nr. $numberOfDoctors -> $doctorName with id: $doctorId \n";
                $doctorsArray[] = [
                    "name" => $doctorName,
                    "link" => $link
                ];
            } else {
                echo "Doctor with $doctorId is already parsed... \n";
            }
            
            array_push($doctorsId, $doctorId);
        }
    } else {
        $retry++;
        echo "Retry $retry ... in 10 seconds \n";
        sleep(10);
        if ($retry == 3) {
            echo "STOP Retrying...\n";
            $keepGoing = false;
        }
    }
    
    $paramsArray['variables']['parameters']['itemOffset'] = $paramsArray['variables']['parameters']['itemOffset'] + 100;
}


foreach ($doctorsArray as $doctor) {
    $pageLink = "https://www.zocdoc.com" . $doctor['link'];
    $doctorPageHTML = new simple_html_dom;
    $doctorPageHTML->load(file_get_contents_curl($pageLink));
    echo "Parsing... " . $doctor['name'] . " page=> " . $doctor['link'] . "\n"; 
    
    preg_match('/window\.__REDUX_STATE__ = JSON\.parse\("(.+?)"\);/', $doctorPageHTML, $output_array);
    
    $doctorJson = json_decode(stripslashes($output_array[1]), TRUE);

    $insurances = $doctorJson['profile']['data']['provider']['inNetworkInsurance'];

    foreach($insurances as $insurance) {
        foreach($insurance['plans'] as $plan) {
            $informationToWrite = $doctor['name'] . "\t" . $insurance['name'] . "\t" . $plan['name'];
            fwrite($fileHandle, $informationToWrite . "\n");
        }
    }
}
//END CRAWLING

// end e-mail information
$timeSpent = time() - $startTime;

echo "Started at: $runDate \n";
echo "Duration: " . number_format($timeSpent / 60, 2) . " minutes \n";
echo "Doctors found: $numberOfDoctors . \n";

fclose($fileHandle);