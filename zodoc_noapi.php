<?php
//Import functions and extenstions
include('scripts/simple_html_dom.php');
include('scripts/functions.php');
ini_set('memory_limit', '512M');
set_time_limit (0);

//Specific script data
$scriptName = "zocdoc_noapi.php";

//common functions and variables
$numberOfDoctors = 0;
$startTime = time();

$runDate = date("Y-m-d H:i:s");

$fileHandle = null;

//BEGIN CRAWLING
$websiteURL = "https://www.zocdoc.com/search?address=New+York%2C+NY&after_5pm=false&before_10am=false&day_filter=AnyDay&dr_specialty=98&filters=%7B%7D&gender=-1&insurance_carrier=-1&insurance_plan=-1&ip=192.160.102.169&language=-1&offset=0&reason_visit=3908&searchType=specialty&search_query=Dentist&sees_children=false&should_insurance_picker_open=false&sort_type=Default";

$fileHandle = newFile($scriptName);
$csvHeader = "Doctor Name" . "\t" . "Insurance Name" . "\t" . "Insurance Plan";
fwrite($fileHandle, $csvHeader . "\n");

$keepGoing = true;
$retry = 0;

$doctorsId = [];
$doctorsArray = [];
$proceduresId = [];

function insideArea($locationInfo) 
{
    $northLatitude = 40.770352;
    $southLatitude = 40.742769;
    $eastLongitude = -73.956122;
    $westLongitude = -74.012601;

    $latitude = $locationInfo['latitude'];
    $longitude = $locationInfo['longitude'];
    if (($latitude >= $southLatitude) && ($latitude <= $northLatitude) && ($longitude <= $eastLongitude) && ($longitude >= $westLongitude)) {
        return true;
    }

    return false;
} 

$mainPageHTML = new simple_html_dom;
$mainPageHTML->load(file_get_contents_curl($websiteURL));

preg_match('/__typename..:..FilterBucket..},.(\"Filter:procedures\.options\.0.+?),.\"Filter:specialties.\"/', $mainPageHTML, $output_array);
$proceduresArray = json_decode("{".stripslashes($output_array[1])."}", TRUE);
var_dump($proceduresArray);

foreach ($proceduresArray as $filter => $procedureInfo) {
    array_push($proceduresId, $procedureInfo['optionId']);
    // echo $procedureInfo['optionId'] . "\n";
}
$indexProcedure = 0;
foreach ($proceduresId as $procedureId) {
    echo "Procedure nr: $indexProcedure. Id: $procedureId \n";
    for($pageNr = 0; $pageNr< 10; $pageNr++) {
        $pageLink = "https://www.zocdoc.com/search?address=New+York%2C+NY&after_5pm=false&before_10am=false&day_filter=AnyDay&dr_specialty=98&filters=%7B%7D&gender=-1&insurance_carrier=-1&insurance_plan=-1&ip=192.160.102.169&language=-1&offset="
        . $pageNr
        . "&reason_visit="
        . $procedureId
        . "&searchType=specialty&search_query=Dentist&sees_children=false&should_insurance_picker_open=false&sort_type=Default";

        $searchPageHTML = new simple_html_dom;
        $searchPageHTML->load(file_get_contents_curl($pageLink));
        if ($searchPageHTML->find('a[data-test="doctor-card-info-name"]')) {
            foreach($searchPageHTML->find('a[data-test="doctor-card-info-name"]') as $doctorLinkHTML) {
                preg_match('/(\/dentist\/.+?)\?/', $doctorLinkHTML->getAttribute('href'), $output_array);
                if (isset($output_array[1])) {
                    $doctorBaseLink = $output_array[1];
                    if (!in_array($doctorBaseLink, $doctorsId)) {
                        array_push($doctorsArray, "https://www.zocdoc.com" . $doctorLinkHTML->getAttribute('href'));
                        array_push($doctorsId, $doctorBaseLink);
                    }
                }
            }
        }
    }
    $indexProcedure++;
}

foreach ($doctorsArray as $pageLink) {
    $doctorPageHTML = new simple_html_dom;
    $doctorPageHTML->load(file_get_contents_curl($pageLink));
    
    preg_match('/window\.__REDUX_STATE__ = JSON\.parse\("(.+?)"\);/', $doctorPageHTML, $output_array);
    
    $doctorJson = json_decode(stripslashes($output_array[1]), TRUE);

    $locations = $doctorJson['profile']['data']['provider']['approvedLocations'];

    $print = false;
    foreach ($locations as $location) {
        if (insideArea($location)) {
            $print = true;
        }
    }

    if ($print) {
        $insurances = $doctorJson['profile']['data']['provider']['inNetworkInsurance'];
        $numberOfDoctors++;
        foreach($insurances as $insurance) {
            foreach($insurance['plans'] as $plan) {
                $informationToWrite = $doctorJson['profile']['data']['provider']['approvedFullName'] . "\t" . $insurance['name'] . "\t" . $plan['name'];
                fwrite($fileHandle, $informationToWrite . "\n");
            }
        }
    } else {
        echo "Nu in patrat " . "\n" . $doctorJson['profile']['data']['provider']['approvedFullName'] . "\n";
    }
}
//END CRAWLING

// end e-mail information
$timeSpent = time() - $startTime;

echo "Started at: $runDate \n";
echo "Duration: " . number_format($timeSpent / 60, 2) . " minutes \n";
echo "Doctors found: $numberOfDoctors . \n";

fclose($fileHandle);