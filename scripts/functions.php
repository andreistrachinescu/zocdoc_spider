<?php

function file_get_contents_curl_post_json($url, $jsonString)
{
    
    $ch = curl_init();
    $torSocks5Proxy = "socks5://127.0.0.1:9050";

    curl_setopt($ch, CURLOPT_PROXY, $torSocks5Proxy);
    curl_setopt($ch, CURLOPT_AUTOREFERER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt(
        $ch,
        CURLOPT_USERAGENT,
        'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/42.0'
    );
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_CAINFO, 'C:\wamp64\www\crawler\scripts\cacert.pem');
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonString);

    //curl_setopt($ch, CURLOPT_VERBOSE, 1);

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}

function file_get_contents_curl($url)
{
    $ch = curl_init();
    $agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/42.0';

    $torSocks5Proxy = "socks5://127.0.0.1:9050";

    curl_setopt($ch, CURLOPT_PROXY, $torSocks5Proxy);
    curl_setopt($ch, CURLOPT_USERAGENT, $agent);
    curl_setopt($ch, CURLOPT_AUTOREFERER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_CAINFO, 'C:\wamp64\www\crawler\scripts\cacert.pem');

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}

function newFile($scriptName)
{
    $path = "Outputs/";
    
    if (file_exists($path)) {
        //do nothing
    } else {
        mkdir($path, 0777, true);
    }

    $fileBaseName = preg_replace("/(.+?).php/", "$1", $scriptName);

    $outputFile = $path . $fileBaseName . "_" . date("Ymd", time()) . ".csv";
    $fileHandle = fopen($outputFile, 'w');

    //Check if is executed via web or via command line from server
    if ($fileHandle === false) {
        $File = "C:\wamp64\www\crawler\\".$outputFile;
        $fileHandle = fopen($File, 'w');
    }
    return $fileHandle;
}


